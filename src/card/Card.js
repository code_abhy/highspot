import React from 'react';
import './Card.scss';

const PLACEHOLDER_IMAGE = 'https://309w5s255371fs4df2vftgbl-wpengine.netdna-ssl.com/wp-content/uploads/2017/09/placeholder-736x414.jpg';

export default class Card extends React.Component {
  render() {
    const imageUrl = this.props.card.imageUrl ?  this.props.card.imageUrl : PLACEHOLDER_IMAGE;
    return (
      <div className="card magic-card">
        <img src={imageUrl} alt={imageUrl}></img>
        <section>
          <div className="magic-card__header">
            <h5 className="magic-card__name text-primary">{this.props.card.name}</h5>
            <label className="magic-card__artist text-secondary">{this.props.card.artist}</label>
          </div>  
          <dl>
            <dt>Set Name</dt>
            <dd>{this.props.card.setName}</dd>
            <dt>Orginal Type</dt>
            <dd>{this.props.card.type}</dd>
          </dl>
        </section>
      </div>
    );
  }
}
