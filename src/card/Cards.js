import React from 'react';
import Card from './Card';

import './Cards.scss';

export default class Cards extends React.Component {
  render() {
    return (
      <div>
        {this.props.cards && this.props.cards.length > 0 &&
          <div className="magic-cards">
            {this.props.cards.map(card => <Card card={card} />)}
          </div>
        }
      </div>
    );
  }
}
