import React from 'react';
import './Filter.scss';

export default class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    this.setState({...this.state, ...nextProps.filter});
  }

  render() {
    return (
      <div className="magic-cards-filter">
        <div className="row">
          <div className="col-md-6">
            <div className="form-group row">
              <label for="name" className="col-sm-2 col-form-label">Name</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="name" placeholder="Enter a name of card" onChange={(e) => this.setState({...this.state, name: e.target.value})}></input>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div class="form-group row">
              <label for="orderBy" className="col-sm-2 col-form-label">Order By</label>
              <div className="col-sm-10">
                <select class="custom-select" id="orderBy" onChange={(e) => this.setState({...this.state, orderBy: e.target.value})}>
                  <option selected value="name">Name</option>
                  <option value="set">Set</option>
                  <option value="artist">Artist</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6" />
          <div className="col-md-6">
            <button type="submit" className="btn btn-primary float-right" onClick={(e) => this.props.onSearch(this.state)}>Search</button>
          </div>
        </div>
      </div>
    );
  }
}
