import React from 'react';
import './Header.scss'

export default class Header extends React.Component {
  render() {
    return (
      <nav className="navbar magic-cards-header">
        <h4 className="navbar-brand text-primary">Highspot Magic Cards</h4>
      </nav>
    )
  }
}
