import React from 'react';
import _ from 'lodash';
import InfiniteScroll from 'react-infinite-scroller';

import './CardsApp.scss';

import Cards from './card/Cards';
import Header from './Header';
import Filter from './Filter';
import Loader from './Loader';

const axios = require('axios').default;

// excerise defaults.
const PAGE_SIZE = 20;
const CARD_TYPE = 'creature';

export default class CardApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [], 
      count: 0, 
      page: 1, 
      pageSize: PAGE_SIZE, 
      hasMoreCards: true, 
      filter: {
        name: '', 
        orderBy: 'name'
    }};
  }

  componentDidMount() {
    this.loadCards();
  }

  loadCards = () => {
    const getURL = `https://api.magicthegathering.io/v1/cards?page=${this.state.page}&pageSize=${PAGE_SIZE}&orderBy=${this.state.filter.orderBy}&name=${this.state.filter.name}&type=${CARD_TYPE}`;
    axios.get(getURL).then((response) => {
      const newCards = response.data.cards;
      let cards = _.isEqual(1, this.state.page) ? newCards : [...this.state.cards, ...newCards];
      this.setState({...this.state, cards, page: this.state.page + 1, hasMoreCards: newCards.length > 0});
    });
  }

  handleSearch = (filter) => {
    this.setState({...this.state, filter, page: 1}, () => {
      this.loadCards(true);
    });
  };

  render() {
    return (
      <InfiniteScroll
        hasMore={this.state.hasMoreCards}
        loader={<Loader />}
        loadMore={this.loadCards}
      >
        <div id="app">
          <Header />
          <Filter
            filter={this.state.filter}
            onSearch={(filter) => this.handleSearch(filter)}
          />
          <Cards
            cards={this.state.cards}
          />
        </div>
      </InfiniteScroll>  
    );
  }  
}
